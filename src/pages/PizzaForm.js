import Page from './Page.js';

export default class PizzaForm extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);
        this.element.querySelector('button[type = "submit"]').addEventListener('click', e => {
            e.preventDefault();
            this.submit(e);
        });
	}

	submit(event) {
        const name = document.querySelector('input[name = "name"]').value;
        if(name == '') {
            alert('Name est vide');
        } else {
            alert(`La pizza ${name} à été ajoutée`);
        }
        console.log(name);
    }
}